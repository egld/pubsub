#!/usr/bin/env python
# coding: utf-8

# In[757]:


class Publisher: 
    def __init__(self, room, msg):
        self.room = room
        self.msg = msg
    
    def send(self):
        bus.publish(self.room, self.msg)        


# In[758]:


class Subscriber:
    def __init__(self, name):
        self.name = name
        
    def notify(self, msg):
        print(f"Новое сообщение {self.name} - {msg}")

    def unsub(self, msg):
        print(f"{self.name} - {msg}")


# In[759]:


class Bus:
    def __init__(self):
        self.rooms = dict()
           
    def publish(self, room, msg):
        if(room in self.rooms):
            self.rooms[room].notify(msg)
        else:
            print("Нету данной комнаты, не могу отправить")
        
    def subscriber(self, sub, room):
        self.rooms.update({room: sub})

    def unsubscriber(self, sub, room):      
        if(sub in self.rooms.values()):
            self.rooms.pop(room)
            sub.unsub("Вы отписаны, комната исчезла")
        else:
            print("Пользователь не найден")


# In[760]:


bus = Bus()
pub = Publisher("2", "work")
pub1 = Publisher("3", "hei")
vasya = Subscriber("vasya")
petr = Subscriber("petr")
bus.subscriber(vasya, "2")
bus.subscriber(petr, "3")
pub1.send()
pub.send()
bus.unsubscriber(vasya, "2")
pub1.send()

# In[ ]:





# In[ ]:





# In[ ]:




